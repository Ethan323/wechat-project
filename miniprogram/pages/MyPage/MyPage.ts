// pages/Mypage/MyPage.ts
Page({
  /**
   * 页面的初始数据
   */


    tapEvent(e: any) {
      console.log(".....................", e);
    },

  data: {
    goods: [
      { title: "商品1", price: 100 },
      { title: "商品2", price: 200 },
      { title: "商品3", price: 300 },
    ],
    serverInfo: {},
    requestData:{r:'from weixin 微信来的申请数据'},

  },
  onTapCallServer() {
    console.log('....call server.....')
    wx.request({
      url: 'http://wwww.localhost:4000/api/req/methods/testPost',
      method: "POST",
      header: {
        "content-type": 'application/x-www-form-urlencoded; charset=UTF-8'
      },
      data: {
        vin: this.data.requestData
      },
      success: (res) => {
        console.log("数据", res.data)
        this.setData({
          serverInfo: res.data
        })
      }
    })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    // 页面创建时执行
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
})


